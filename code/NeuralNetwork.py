from WeightInitializer import *
from WeightRegression import *
import time

import matplotlib.pyplot as plt
import seaborn as sn
"""
Vanilia neural network:


layers = list of the number of neurones by layers 
[input, hidden1, ..., hiddenN, output]
error_func = error/cost function
act_func = activation function
out_func = output function
gb_method = gradient descent method
w_init = weight initializer method
reg = regression method 
dropout_prob = proportion of activated weights by layer

"""

class NeuralNetwork:
    def __init__(self, layers, error_func, act_func,
                 out_func, gd_method, w_init=standard_initializer, dropout_prob=1,
                 rand_gen=np.random.RandomState()):
        t = time.time()
        self.dropout_prob = dropout_prob
        self.input = layers[0]
        self.output = layers[-1]
        self.e_func = error_func
        self.a_func = act_func
        self.o_func = out_func
        self.layers = layers
        self.weights, self.biases = w_init(layers, rand_gen)
        self.gd_method = gd_method
        self.reg = gd_method.reg
        self.rand_g = rand_gen

        print("NN initial phase: Ok {0}s".format(time.time() -t))

        """
        compute the error gradient for the cost function
        """
    def to_string(self):
        to_string_layers = '('
        for x in self.layers[:-1]:
            to_string_layers += str(x)+','
        to_string_layers += str(self.layers[-1])+')'
        return '{0}_{1}_{2}_{3}_{4}_{5}'.format(to_string_layers, self.e_func['name'],
                                                self.a_func['name'], self.o_func['name'],
                                                self.gd_method.to_string(), self.dropout_prob)
    def error_grad(self, labels, pre_act, act):
        if self.e_func['name'] == 'quadratic':
            return self.e_func['deriv'](act[-1], labels) \
                   * self.o_func['deriv'](pre_act[-1])
        if self.e_func['name'] == 'cross_entropy':
            return self.e_func['deriv'](act[-1], labels)
        """
        predict y from x
        """
    def predict(self, x):
        x = x[:, np.newaxis]
        for w, b in zip(self.weights[:-1], self.biases[:-1]):
            x = self.a_func['id'](np.dot(w, x) + b)
        return self.o_func['id'](np.dot(self.weights[-1], x)
                                 + self.biases[-1]).ravel()
    """
    Compute the total gradient for each weights and biases.
    """
    def compute_errors(self, data):
        X = data[0]
        y = data[1]
        error_w = [np.empty(weight.shape, dtype=float) for weight in self.weights]
        error_b = [np.empty(biase.shape, dtype=float) for biase in self.biases]

        if self.dropout_prob != 1:
            drop_w = [(self.rand_g.rand(weight.shape[0], weight.shape[1]) < self.dropout_prob) / self.dropout_prob
                      for weight in self.weights]
        else:
            drop_w = [np.ones(weight.shape) for weight in self.weights]

        act = [np.empty((x, 1), dtype=float) for x in self.layers]
        pre_act = [np.empty((x, 1), dtype=float) for x in self.layers[1:]]

        act[0] = X[:, np.newaxis]
        y = y[:, np.newaxis]

        layer_count = 1
        for w, b, drop in zip(self.weights[:-1], self.biases[:-1], drop_w):
            pre_act[layer_count - 1] = np.dot(w*drop, act[layer_count-1]) + b
            act[layer_count] = self.a_func['id'](pre_act[layer_count-1])
            layer_count += 1
        pre_act[layer_count - 1] = \
            np.dot(self.weights[-1]*drop_w[-1], act[layer_count - 1]) + self.biases[-1]

        act[layer_count] = self.o_func['id'](pre_act[layer_count - 1])
        delta = self.error_grad(y, pre_act, act)

        error_b[-1] = delta
        error_w[-1] = np.dot(delta, act[-2].T)*drop_w[-1]
        for i in range(2, len(self.layers)):
            delta = np.dot(self.weights[-i+1].T, delta) \
                    * self.a_func['deriv'](pre_act[-i])

            error_b[-i] = delta
            error_w[-i] = np.dot(delta, act[-i-1].T)*drop_w[-i]
        return error_b, error_w

    def fit(self, train_data, epoch, mini_batch, valid_data=None, accuracy=False, cost=False, output=False):
        valid_acc = np.empty(epoch, dtype=float)
        train_acc = np.empty(epoch, dtype=float)
        valid_cost = np.empty(epoch, dtype=float)
        train_cost = np.empty(epoch, dtype=float)

        nb_entries = len(train_data)
        for j in range(epoch):
            self.rand_g.shuffle(train_data)
            for i in range(0, len(train_data), mini_batch):
                self.fit_mini_batch(train_data[i:i + mini_batch], nb_entries)
            if output:
                print("Epoch n°{0}".format(j))
            if valid_data:
                if accuracy:
                    valid_prec = self.accuracy_class(valid_data)
                    train_prec = self.accuracy_class(train_data)
                    valid_acc[j] = valid_prec
                    train_acc[j] = train_prec
                    if output:
                        print("Classification scores:\n"
                              "train set = {0}\n"
                              "test set = {1}\n"
                              .format(train_prec, valid_prec))

                if cost:
                    v_cost = self.cost(valid_data)
                    t_cost = self.cost(train_data)
                    valid_cost[j] = v_cost
                    train_cost[j] = t_cost
                    if output:
                        print("Cost scores:\n"
                              "train set = {0}\n"
                              "test set = {1}\n"
                              .format(t_cost, v_cost))
        if output:
            self.print_result(epoch, valid_acc, train_acc, valid_cost, train_cost)
        square_diff_acc = (train_acc - valid_acc)**2
        square_diff_cost = (train_cost - valid_cost)**2
        return train_acc, valid_acc, square_diff_acc, \
               train_cost, valid_cost, square_diff_cost

    def fit_mini_batch(self, mini_batch, nb_entries):
        # compute the gradient descent for the all sample
        len_mb = len(mini_batch)
        grads = [self.compute_errors(entry) for entry in mini_batch]
        b, w = np.sum(grads, axis=0)
        w /= len_mb
        b /= len_mb
        self.weights, self.biases = \
            self.gd_method.train(self.weights, w, self.biases, b, nb_entries)

    def cost(self, dataset):
        cost = np.sum([self.e_func['id'](self.predict(set[0]), set[1]) for set in dataset])
        # reg = self.reg.cost(self.weights, self.biases)
        return 1/len(dataset)*cost

    def accuracy_class(self, dataset):
        accuracy = 0
        for set in dataset:
            pred = self.predict(set[0])
            arg_pred = np.argmax(pred)
            arg_label = np.argmax(set[1])
            if arg_pred == arg_label:
                accuracy += 1
        return accuracy/len(dataset)

    def save(self, filename):
        np.save('./networks/nn_{filename}_weights.npy'.format(filename=filename), self.weights)
        np.save('./networks/nn_{filename}_biases.npy'.format(filename=filename), self.biases)

    def load(self, filename):
        self.weights = np.load('./networks/nn_{filename}_weights.npy'.format(filename=filename))
        self.biases = np.load('./networks/nn_{filename}_biases.npy'.format(filename=filename))

    def print_result(self, epoch, valid_acc, train_acc, valid_cost, train_cost):
        fig = plt.figure(1)
        ax = fig.add_subplot(211)
        ax.plot(range(epoch), valid_acc)
        ax.plot(range(epoch), train_acc)
        ax.set_xlim([0, epoch])
        ax.grid(True)
        ax.set_title('Classification accuracy\n{nb} classes'.format(nb=self.layers[-1]))
        ax.set_ylabel('Accuracy')
        ax.set_xlabel('epoch')
        ax.legend(['Validation', 'training'])

        ax = fig.add_subplot(212)
        ax.plot(range(epoch), valid_cost)
        ax.plot(range(epoch), train_cost)
        ax.set_xlim([0, epoch])
        ax.grid(True)
        ax.set_title('Error score: {function}'.format(function=self.e_func['name']))
        ax.set_ylabel('Cost')
        ax.set_xlabel('epoch')
        ax.legend(['Validation', 'training'])
        plt.tight_layout()
        plt.show()
