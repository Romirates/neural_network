#! /usr/bin/env python3
from sklearn import datasets
from sklearn.preprocessing import LabelBinarizer
from NeuralNetwork import *
from NNFunctions import *
from NNUtilities import *
from NNModelsBuilder import *
import math

from NNGD import *
from NNChooser import *
X, y = datasets.load_digits(return_X_y=True)

lb = LabelBinarizer()
lb.fit(y)
y = lb.transform(y)
data = [[x, y] for x, y in zip(X, y)]
np.random.shuffle(data)
cut = math.ceil(len(data)*0.8)
train = data[:cut]
valid = data[cut:]
models = []
epoch = 100
lr = 0.01
ld = 20
mini_batch = 1
layers = [len(X[0]), 100, len(y[0])]
adadelta1 = ADADELTA(layers, 0.90, 1e-5,  NoReg())
new_gen = np.random.RandomState(42)
"""
HP : gamma = [0.96288259] epsi = 1e-05
HP : gamma = [0.95670575] epsi = 1e-05
nn1 = NeuralNetwork(layers, act_func=sigmoid,
                   out_func=softmax_f, error_func=cross_ent,
                   w_init=normalized_variance, gd_method=adadelta1, dropout_prob=1)
nn1.fit(train, epoch, 1, valid, True, True, True)
"""
epsis = [24, 15, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
epsi = BasicChooser(epsis, len(epsis))
gamma = BasicChooser([0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99])
# gamma = ReversePowChooser(15, 0, 16)
builder = Adadelta_builder(epsi, gamma, 42, 18, layers)
builder.build(10, train, valid)
df = builder.get_df()
plot_df(df)
