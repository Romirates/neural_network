from NNGD import *
from NNFunctions import *
from NeuralNetwork import *
import pandas as pd
class Adadelta_builder:
    def __init__(self, epsi_chooser, gamma_chooser, seed, nb_models, layers):
        self.seed = int(seed)
        self.epsi_chooser = epsi_chooser
        self.gamma_chooser = gamma_chooser
        self.nb_models = nb_models
        self.layers = layers
        self.models = {}
        self.data = []

    def build(self, epoch, train_data, valid_data):
        while not self.epsi_chooser.finish():
            epsi_pow = self.epsi_chooser.next_value()
            epsi = 10 ** -float(epsi_pow)
            epsi_pow = '1e^-'+str(epsi_pow)
            for i in range(self.nb_models):
                gene = np.random.RandomState(self.seed)
                gamma = self.gamma_chooser.next_value()
                ada = ADADELTA(self.layers, gamma, epsi, NoReg())
                nn = NeuralNetwork(self.layers, act_func=sigmoid,
                       out_func=softmax_f, error_func=cross_ent,
                       w_init=normalized_variance, gd_method=ada, dropout_prob=1, rand_gen=gene)
                metrics = nn.fit(train_data, epoch, 1, valid_data, True, True, False)
                self.models[nn.to_string()] = metrics[3][-1]
                score = metrics[3][-1]
                self.data.append([gamma, score, 15 - i, epsi_pow])

    def get_df(self):
        return pd.DataFrame(self.data, columns=['x', 'y', 'gamma_pow', 'epsilon'])








