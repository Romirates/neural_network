    Neural network

  Authors
  -------
  Romain Ferrand
  Marco Freire

  What is it?
  -----------

  This program is an implementation of 
a vanilla neural network in python/numpy.


Implemented techniques
----------------------
    Activation function
    -------------------
    relu
    sigmoid
    tanh
    softplus
    
    Output function
    ---------------
    softmax (softmax_f)

    Cost function
    -------------
    cross entropy (cross_ent)
    L2

    Weight regularization
    ---------------------
    WeightDecay(lambda)
    WeightElimination(lambda)
    dropout=p p ∈ ]0,1[
    p is the proportion of 
    activated weights by layer

    Gradient descent method
    -----------------------
    Stochastic gradient descent:
    SGD(learning_rate, weight_regularization)
    ADADELTA(layers, p, epsi, weight_regularization)


  How to use it?
  -------------
  You can already run:
    -spam_test.py
    -mnist_test.py
In these files you can configure a neural network:
NeuralNetwork(layers, activation_function, 
                output_function, cost_function, 
                weight_initializer, gradient_descent_method, dropout_proportion)
Then call the method fit(train_set, epoch, 
                                    mini_batch_length, valid_set, accuracy=(True or false),
                                    cost=(True or False), output=(true or false))
accuracy = True
    - compute the accuracy score
cost = True
    - compute the cost score
output = True
    - plot the accuracy and cost score


